# JUCE

This is a Conan recipe for the *JUCE* framework.  The main repository can be found [here](https://github.com/juce-framework/JUCE).

### Usage

#### Create the Package

```
git clone https://gitlab.com/sturd/conan-juce.git
cd conan-juce
conan create . --user [YOUR_PACKAGE_USER] --channel [YOUR_PACKAGE_CHANNEL]
```

#### Consuming the Package

In your project `conanfile.py`, add JUCE to your requires list and pass the JUCE `package_folder` to the `CMAKE_PREFIX_PATH` cache variable.

``` python
requires = [
    juce/7.0.5@[YOUR_PACKAGE_USER]/[YOUR_PACKAGE_CHANNEL]
]

def generate(self):
    deps = CMakeDeps(self)
    deps.generate()
    tc = CMakeToolchain(self)
    tc.cache_variables["CMAKE_PREFIX_PATH"] = self.dependencies["juce"].package_folder

```

For adding JUCE to the `CMakeLists.txt` files which rely on it, follow the instructions in the [JUCE docs](https://github.com/juce-framework/JUCE/blob/7.0.5/docs/CMake%20API.md).
